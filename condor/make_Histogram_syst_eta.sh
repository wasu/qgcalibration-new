#!/bin/bash
source /afs/cern.ch/work/w/wasu/etas-analysis/newframe/env/bin/activate

workdir=/afs/cern.ch/work/w/wasu/NewCodes/condor/scripts
outputdir=/eos/user/w/wasu/conda/NewCodes
#outputdir=/eos/atlas/atlascerngroupdisk/perf-jets/TAGGING/ANA-JETM-2020-02/processed_sample
sampledir=/eos/atlas/atlascerngroupdisk/perf-jets/TAGGING/ANA-JETM-2020-02/sample_Dec11

period=${1}
syst=${2}

python -u ${workdir}/make_Histogram_Pythia_eta.py --path ${sampledir}/pythia/pythia${period} --period ${period} --output-path ${outputdir}/Results_eta --mc-syst-type ${syst} 

#python -u ${workdir}/make_Histogram_Data.py --path ${sampledir}/data/ --period ${period} --output-path ${outputdir}/Processed_Samples_Data_Oct18_syst/${syst} --reweight-file-path ${outputdir}/Processed_Samples_Pythia_Nov8_syst/${syst}
