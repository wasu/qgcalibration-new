#!/bin/bash
source /eos/user/h/haoran/miniconda3/bin/activate ml

workdir=/afs/cern.ch/user/h/haoran/qgcal/EB3/condor/scripts
outputdir=/eos/atlas/atlascerngroupdisk/perf-jets/TAGGING/ANA-JETM-2020-02/processed_sample
sampledir=/eos/atlas/atlascerngroupdisk/perf-jets/TAGGING/ANA-JETM-2020-02/sample_new_smalltest

period=${1}
tree=${2}

python -u ${workdir}/make_Histogram_Pythia_syst_JES.py --path ${sampledir}/pythia_JES/pythia${period} --period ${period} --output-path ${outputdir}/Processed_Samples_Pythia_Nov8_syst_JES --tree ${tree}

python -u ${workdir}/make_Histogram_Data.py --path ${sampledir}/data/data${period} --period ${period} --output-path ${outputdir}/Processed_Samples_Data_Oct18_syst/tree_${tree}/nominal --reweight-file-path ${outputdir}/Processed_Samples_Pythia_Nov8_syst_JES/tree_${tree}/nominal 
