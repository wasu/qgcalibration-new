#!/bin/bash
#source /eos/user/w/wasu/conda/test/bin/activate
source /afs/cern.ch/work/w/wasu/etas-analysis/newframe/env/bin/activate
#cd /eos/user/w/wasu/conda/NewCodes
#python make_Histogram_Pythia_JES.py --path /eos/atlas/atlascerngroupdisk/perf-jets/TAGGING/ANA-JETM-2020-02/sample_new/pythia/pythiaA --period A --output-path ./Results_JES --tree nominal
python make_Histogram_Pythia_pdf.py --path $1 --period $2 --output-path $3 --mc-syst-type nominal

