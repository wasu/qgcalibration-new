#!/bin/bash
#source /eos/user/w/wasu/conda/test/bin/activate
source /afs/cern.ch/work/w/wasu/etas-analysis/newframe/env/bin/activate
#cd /eos/user/w/wasu/conda/NewCodes
#python make_Histogram_Pythia_JES.py --path /eos/atlas/atlascerngroupdisk/perf-jets/TAGGING/ANA-JETM-2020-02/sample_new/pythia/pythiaA --period A --output-path ./Results_JES --tree nominal
tree=${1}
python /afs/cern.ch/work/w/wasu/NewCodes/Calculate_SF.py --path-mc /eos/user/w/wasu/conda/NewCodes/Results_JES/ADE/${tree}/dijet_pythia_all.root --path-data /eos/atlas/atlascerngroupdisk/perf-jets/TAGGING/ANA-JETM-2020-02/processed_sample/Processed_Samples_Data_Oct18/dijet_data_all.root --period ADE --reweighting quark --output-path /eos/user/w/wasu/conda/NewCodes/syst_JES/${tree} 

